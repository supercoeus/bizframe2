
package org.tinygroup.bizframe.action.exception;

import org.tinygroup.exception.BaseRuntimeException;

/**
 *
 */
public class BizRuntimeException extends BaseRuntimeException{

	private static final long serialVersionUID = 1L;

	public BizRuntimeException(String errorCode, String errorMsg){
		super(errorCode, errorMsg);
	}
}