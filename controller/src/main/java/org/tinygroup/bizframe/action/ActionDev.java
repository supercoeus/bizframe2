package org.tinygroup.bizframe.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller()
public class ActionDev {

    @RequestMapping(value = "/{pageId}", method = RequestMethod.GET)
    public String showPageView(@PathVariable String pageId) {
        return pageId;
    }

    @RequestMapping(value = "/{pageId2}/{pageId}", method = RequestMethod.GET)
    public String showPageView1(@PathVariable String pageId2, @PathVariable String pageId) {
        return pageId2 + "/" + pageId;
    }

    @RequestMapping(value = "/{partId1}/{pageId}/{pageId2}", method = RequestMethod.GET)
    public String showPageSubView(@PathVariable String partId1, @PathVariable String pageId, @PathVariable String pageId2) {
        return partId1+"/" + pageId + "/" + pageId2;
    }

    @RequestMapping(value = "/{partId1}/{pageId}/{pageId2}/{pageId3}", method = RequestMethod.GET)
    public String showPageSubView2(@PathVariable String partId1, @PathVariable String pageId, @PathVariable String pageId2, @PathVariable String pageId3) {
        return partId1+"/" + pageId + "/" + pageId2+"/"+pageId3;
    }

    @RequestMapping({"", "/"})
    public String dodo() {
        return "index";
    }


}
