package org.tinygroup.bizframe.service.inter.dto.complex;

import java.io.Serializable;

/**
 * 权限关联查询需要用到的dto
 * Created by Mr.wang on 2016/7/23.
 */
public class TRightRefDto implements Serializable{
    /**
	 * long
	 */
	private static final long serialVersionUID = 1030816682686180487L;

    private String modelCode;
    
    private String kindCode;

    private Integer id;

	public String getKindCode() {
		return kindCode;
	}

	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
