package org.tinygroup.bizframe.service.impl;

import org.tinygroup.bizframe.basedao.util.PageResponseAdapter;
import org.tinygroup.bizframe.business.inter.SysUserLoginBusiness;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.common.util.BeanUtil;
import org.tinygroup.bizframe.common.util.CamelCaseUtil;
import org.tinygroup.bizframe.dao.inter.constant.TsysUserLoginTable;
import org.tinygroup.bizframe.dao.inter.pojo.TsysUser;
import org.tinygroup.bizframe.dao.inter.pojo.TsysUserLogin;
import org.tinygroup.bizframe.service.inter.SysUserLoginService;
import org.tinygroup.bizframe.service.inter.dto.SysUserDto;
import org.tinygroup.bizframe.service.inter.dto.SysUserLoginDto;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * 用户登录服务
 * Created by Mr.wang on 2016/7/14.
 */
public class SysUserLoginServiceImpl implements SysUserLoginService {
    private SysUserLoginBusiness sysUserLoginBusiness;

    public SysUserLoginBusiness getSysUserLoginBusiness() {
        return sysUserLoginBusiness;
    }

    public void setSysUserLoginBusiness(SysUserLoginBusiness sysUserLoginBusiness) {
        this.sysUserLoginBusiness = sysUserLoginBusiness;
    }

    public SysUserLoginDto addSysUserLogin(SysUserLoginDto sysUserLoginDto) {
        TsysUserLogin tsysUserLogin = BeanUtil.copyProperties(TsysUserLogin.class, sysUserLoginDto);
        return BeanUtil.copyProperties(SysUserLoginDto.class, sysUserLoginBusiness.add(tsysUserLogin));
    }

    public int updateSysLogin(SysUserLoginDto sysUserLoginDto) {
        TsysUserLogin tsysUserLogin = BeanUtil.copyProperties(TsysUserLogin.class, sysUserLoginDto);
        return sysUserLoginBusiness.update(tsysUserLogin);
    }

    public int deleteSysUserLogin(String depCode) {
        return sysUserLoginBusiness.deleteByKeys(depCode);
    }

    public PageResponse getSysUserLoginPager(PageRequest pageRequest, SysUserLoginDto sysUserLoginDto) {
        TsysUserLogin tsysUserLogin = BeanUtil.copyProperties(TsysUserLogin.class, sysUserLoginDto);

        String sortField = pageRequest.getSort();
        if (StringUtil.isEmpty(sortField)) {
            sortField = CamelCaseUtil.getFieldName("userId");
        }
        String orderByField = CamelCaseUtil.getFieldName(sortField);
        OrderBy orderBy = new OrderBy(orderByField, "asc".equalsIgnoreCase(pageRequest.getOrder()));
        Pager<TsysUserLogin> pager = sysUserLoginBusiness.getPager(pageRequest.getStart(), pageRequest.getPageSize(), tsysUserLogin, orderBy);
        return PageResponseAdapter.transform(pager);
    }

    /**
     * @param sysUserDto
     * @return
     */
    public SysUserLoginDto getSysUserLoginByUser(SysUserDto sysUserDto) {
        TsysUser tsysUser = BeanUtil.copyProperties(TsysUser.class, sysUserDto);
        TsysUserLogin tsysUserLogin = sysUserLoginBusiness.getByUser(tsysUser);
        if (tsysUserLogin != null) {
            return BeanUtil.copyProperties(SysUserLoginDto.class, tsysUserLogin);
        }
        return null;
    }

}
