package org.tinygroup.bizframe.business.inter;

import java.util.List;

import org.tinygroup.bizframe.dao.inter.pojo.TreeData;
import org.tinygroup.bizframe.dao.inter.pojo.TsysKind;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Mr.wang on 2016/7/14.
 */
public interface SysKindBusiness {
	TsysKind getById(String kindCode);

    Pager<TsysKind> getPager(int start, int limit, TsysKind sysKind, final OrderBy... orderBies);

    TsysKind add(TsysKind sysKind);

    int update(TsysKind sysKind);

    int deleteByKeys(String... pks);

    boolean checkExists(TsysKind sysKind);
    
    List getKindTree(TreeData tree);
    
    List getKindsList(TsysKind sysKind);
}
