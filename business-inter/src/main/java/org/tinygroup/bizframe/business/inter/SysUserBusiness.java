package org.tinygroup.bizframe.business.inter;

import org.tinygroup.bizframe.dao.inter.pojo.TsysOfficeUser;
import org.tinygroup.bizframe.dao.inter.pojo.TsysUser;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

import java.util.List;

/**
 * Created by Mr.wang on 2016/7/14.
 */
public interface SysUserBusiness {
    TsysUser getById(String id);

    int deleteByKeys(String... pks);

    Pager<TsysUser> getPager(int start, int limit, TsysUser tsysUser, final OrderBy... orderBies);

    TsysUser add(TsysUser tsysUser);

    int update(TsysUser tsysUser);

    boolean checkExists(TsysUser tsysUser);

    List<TsysOfficeUser> getTsysOfficeUserList(TsysOfficeUser tsysOfficeUser);

    Pager getAllInfoPager(int start, int pageSize, TsysUser sysUser, final OrderBy... orderBies);
    
    int updateLockStatusByKeys(String[] pks,String lockStatus);

    int resetPwd(String defaultPwd, String... userIds);

    boolean validate(String userName, String password);
}
