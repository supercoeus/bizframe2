package org.tinygroup.bizframe.business.inter;

import org.tinygroup.bizframe.dao.inter.pojo.complex.TRightRef;
import org.tinygroup.bizframe.ext.dao.inter.pojo.TsysUserRight;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Mr.wang on 2016/7/14.
 */
public interface SysUserRightBusiness {
	TsysUserRight getById(Integer id);

    Pager<TsysUserRight> getPager(int start, int limit, TsysUserRight sysUserRight, final OrderBy... orderBies);

    TsysUserRight add(TsysUserRight sysUserRight);

    int update(TsysUserRight sysUserRight);

    int deleteByKeys(Integer... pks);

    boolean checkExists(TsysUserRight sysUserRight);

    int[] grantRights(String userId,Integer[] menuIds, String createBy);

    int revokeRights(String userCode,Integer[] subTransIds);

    Pager<TRightRef> queryRightsInUser(int start, int pageSize, String roleCode, TRightRef tRightRef, Boolean isAssigned);



}
